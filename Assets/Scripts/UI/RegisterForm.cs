﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SpaceGrinder.UI {
	public class RegisterForm : MonoBehaviour {
		[SerializeField] private TMP_InputField m_Username, m_Email, m_Password;
		[SerializeField] private TMP_Text m_MessageText;
		[SerializeField] private Button m_LoginButton, m_RegisterButton;
		[SerializeField] private GameObject m_LoginScreen, m_MainScreen, m_LoadingIcon;

		public void DoRegister() {
			OnStartAction();

			APIManager.Instance.Register(m_Username.text, m_Email.text, m_Password.text, (result, message) => {
				OnEndAction();

				if (result == APIManager.RegisterResult.Success) {
					m_LoginScreen.SetActive(false);
					m_MainScreen.SetActive(true);
				} else {
					m_MessageText.text = message;
				}
			});
		}

		public void OnStartAction() {
			m_RegisterButton.interactable = false;
			if (m_LoginButton != null) {
				m_LoginButton.interactable = false;
			}
			m_LoadingIcon.SetActive(true);
			m_MessageText.text = "";
		}

		public void OnEndAction() {
			m_RegisterButton.interactable = true;
			if (m_LoginButton != null) {
				m_LoginButton.interactable = true;
			}
			m_LoadingIcon.SetActive(false);
		}
	}
}
