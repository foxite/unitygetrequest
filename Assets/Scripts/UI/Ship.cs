﻿using Newtonsoft.Json;

namespace SpaceGrinder.UI {
	public class Ship {
		public int id;
		[JsonProperty(PropertyName = "hull_upgrades")]          public int hullUpgrades;
		[JsonProperty(PropertyName = "armor_upgrades")]         public int armorUpgrades;
		[JsonProperty(PropertyName = "speed_upgrades")]         public int speedUpgrades;
		[JsonProperty(PropertyName = "kinetic_upgrades")]       public int kineticUpgrades;
		[JsonProperty(PropertyName = "laser_upgrades")]         public int laserUpgrades;
		[JsonProperty(PropertyName = "missile_upgrades")]       public int missileUpgrades;
		[JsonProperty(PropertyName = "missile_count_upgrades")] public int missileCountUpgrades;
		[JsonProperty(PropertyName = "type_name")]              public string typeName;
		[JsonProperty(PropertyName = "class")]                  public int shipClass;
	}

	public class ShipType {
		public int id;
		public string name;
		[JsonProperty(PropertyName = "class")] public int classNumber;
		public int cost;
	}
}
