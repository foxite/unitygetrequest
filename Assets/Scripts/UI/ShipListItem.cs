﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SpaceGrinder.UI {
	public class ShipListItem : MonoBehaviour {
		[SerializeField] private Image m_Background, m_Image;
		[SerializeField] private TMP_Text m_Title, m_Class;
		[SerializeField] private TMP_Text m_Hull, m_Armor, m_Kinetic, m_Laser, m_Missile, m_MissileCount, m_Speed;
		[SerializeField] private Button m_PlayButton, m_StarButton, m_UpgradeButton, m_RemoveButton;

		private ShipSelection m_Selection;
		private Ship m_Ship;

		private void Start() {
			m_PlayButton.onClick.AddListener(() => {
				if (m_Selection != null && m_Ship != null) {
					m_Selection.SetSelection(m_Ship);
				}
			});
		}

		public void Initialize(Ship ship, ShipSelection selection, MainScreen mainScreen, UpgradeScreen upgradeScreen, Color bgColor) {
			m_Title.text = ship.typeName + " " + ship.id;
			m_Class.text = "Class " + ship.shipClass + " Fighter";

			m_Hull.text = "Hull: " + ship.hullUpgrades;
			m_Armor.text = "Armor: " + ship.armorUpgrades;
			m_Kinetic.text = "Kinetic: " + ship.kineticUpgrades;
			m_Laser.text = "Laser: " + ship.laserUpgrades;
			m_Missile.text = "Missile: " + ship.missileUpgrades;
			m_MissileCount.text = "Missile count: " + ship.missileCountUpgrades;
			m_Speed.text = "Speed: " + ship.speedUpgrades;

			m_Ship = ship;
			m_Selection = selection;

			m_Background.color = bgColor;

			m_UpgradeButton.onClick.AddListener(() => {
				upgradeScreen.SetShip(m_Ship);
				mainScreen.gameObject.SetActive(false);
				upgradeScreen.gameObject.SetActive(true);
			});
		}
	}
}
