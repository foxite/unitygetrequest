﻿using SpaceGrinder.Game;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SpaceGrinder.UI {
	public class MainScreen : MonoBehaviour {
		[SerializeField] private GameObject m_ShipListItemPrefab;
		[SerializeField] private GameObject m_MainScreen, m_LoginScreen, m_LoadingScreen;
		[SerializeField] private GameObject m_LogoutLoadingIcon, m_ListLoadingIcon, m_PlayLoadingIcon;
		[SerializeField] private Transform m_ShipList;
		[SerializeField] private TMP_Text m_ShipListErrorText;
		[SerializeField] private Button m_LogoutButton;
		[SerializeField] private ShipSelection m_Selection;
		[SerializeField] private Color m_OddColor, m_EvenColor;
		[SerializeField] private UpgradeScreen m_UpgradeScreen;

		public void LogOut() {
			m_LogoutButton.interactable = false;
			m_LogoutLoadingIcon.SetActive(true);

			APIManager.Instance.Logout((result, str) => {
				if (result != APIManager.LogoutResult.Success) {
					Debug.LogWarning("An error was encountered while logging out: " + result.ToString() + " : " + str);
				}

				// Log out regardless
				m_MainScreen.SetActive(false);
				m_LoginScreen.SetActive(true);
				m_LogoutLoadingIcon.SetActive(false);
				m_LogoutButton.interactable = true;
			});
		}

		private void OnEnable() {
			m_ListLoadingIcon.SetActive(true);

			for (int i = 0; i < m_ShipList.childCount; i++) {
				Destroy(m_ShipList.GetChild(i).gameObject);
			}

			APIManager.Instance.GetShips((success, message, ships) => {
				m_ListLoadingIcon.SetActive(false);
				if (success) {
					if (ships.Length == 0) {
						m_ShipListErrorText.text = "You have no ships.";
					} else {
						for (int i = 0; i < ships.Length; i++) {
							GameObject listItem = Instantiate(m_ShipListItemPrefab, m_ShipList);
							ShipListItem sli = listItem.GetComponent<ShipListItem>();
							sli.Initialize(ships[i], m_Selection, this, m_UpgradeScreen, i % 2 == 0 ? m_EvenColor : m_OddColor);
						}
					}
				} else {
					m_ShipListErrorText.text = message;
				}
			});
		}

		public void Play() {
			m_PlayLoadingIcon.SetActive(true);
			m_LogoutButton.interactable = false;

			StartCoroutine(Util.RunAfterSeconds(1f, () => {
				m_MainScreen.SetActive(false);
				m_PlayLoadingIcon.SetActive(false);
				Player.Instance.gameObject.SetActive(true);
			}));
			
			Player.Instance.StartCoroutine(Util.RunAfterSeconds(5f, () => {
				EndGame();
			}));
		}

		public void EndGame() {
			m_LoadingScreen.SetActive(true);
			APIManager.Instance.EndGame(1234, (success, message, newMoney) => {
				m_LoadingScreen.SetActive(false);
				gameObject.SetActive(true);
				Player.Instance.gameObject.SetActive(false);
			});
		}
	}
}
