﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SpaceGrinder.UI {
	public class LoginForm : MonoBehaviour {
		[SerializeField] private TMP_InputField m_Username, m_Password;
		[SerializeField] private TMP_Text m_MessageText;
		[SerializeField] private Button m_LoginButton, m_RegisterButton;
		[SerializeField] private GameObject m_LoginScreen, m_MainScreen, m_LoadingIcon;

		private void Start() {
			OnStartAction();

			APIManager.Instance.IsLoggedIn((result, str) => {
				OnEndAction();

				if (result == APIManager.LoginTestResult.Active || result == APIManager.LoginTestResult.LastSessionInactive) {
					m_LoginScreen.SetActive(false);
					m_MainScreen.SetActive(true);
				}
			});
		}

		public void DoLogin() {
			OnStartAction();

			APIManager.Instance.Login(m_Username.text, m_Password.text, (result, message) => {
				OnEndAction();

				if (result == APIManager.LoginResult.Success) {
					m_LoginScreen.SetActive(false);
					m_MainScreen.SetActive(true);
				} else {
					m_MessageText.text = message;
				}
			});
		}

		public void OnStartAction() {
			m_LoginButton.interactable = false;
			if (m_RegisterButton != null) {
				m_RegisterButton.interactable = false;
			}
			m_LoadingIcon.SetActive(true);
			m_MessageText.text = "";
		}

		public void OnEndAction() {
			m_LoginButton.interactable = true;
			if (m_RegisterButton != null) {
				m_RegisterButton.interactable = true;
			}
			m_LoadingIcon.SetActive(false);
		}
	}
}
