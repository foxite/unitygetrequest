﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SpaceGrinder.UI {
	public class APIManager : MonoBehaviour {
		public static APIManager Instance { get; private set; }

		private string m_LoginToken;

		public int Money { get; set; }

		private void Awake() {
			Instance = this;

			Money = 639854;

			string configFile = Path.Combine(Application.persistentDataPath, "api.json");

			if (File.Exists(configFile)) {
				APISettings settings = JsonConvert.DeserializeObject<APISettings>(File.ReadAllText(configFile));
				m_LoginToken = settings.authToken;
			}
		}

		/// <summary>
		/// Posts data to an API path.
		/// </summary>
		private IEnumerator PostData(string path, IDictionary<string, string> data, bool includeToken, Action<WWW> callback) {
			string url = "http://localhost/edsa-game/" + path;

			if (includeToken) {
				if (data == null) {
					data = new Dictionary<string, string>() {
					{ "token", m_LoginToken }
				};
				} else {
					data.Add("token", m_LoginToken);
				}
			}

			WWWForm form = new WWWForm();
			foreach (KeyValuePair<string, string> item in data) {
				form.AddField(item.Key, item.Value);
			}

			WWW www = new WWW(url, form);

			yield return www;

			callback(www);
		}

		#region Account functions
		/// <param name="callback">string is the human-readable error message, or null if successful</param>
		public void Register(string username, string email, string password, Action<RegisterResult, string> callback) {
			Dictionary<string, string> data = new Dictionary<string, string>(3) {
			{ "username", username },
			{ "email",    email },
			{ "password", password }
		};

			StartCoroutine(PostData("accounts/register.php", data, false, (www) => {
				try {
					JObject response = JObject.Parse(www.text);
					if (response["success"].Value<bool>()) {
						m_LoginToken = response["data"].Value<string>();
						callback(RegisterResult.Success, null);
					} else {
						RegisterResult result;
						switch (response["reason"].Value<string>()) {
						case "email_exists":
							result = RegisterResult.EmailTaken;
							break;
						case "username_exists":
							result = RegisterResult.UsernameTaken;
							break;
						case "client_error":
						case "server_error":
						default:
							result = RegisterResult.Error;
							break;
						}
						callback(result, response["message"].ToObject<string>());
					}
				} catch (JsonException ex) {
					callback(RegisterResult.Error, "Error communicating with server. (C1-1)");
					Debug.LogException(ex, this);
				}
			}));
		}

		/// <param name="callback">string is the human-readable error message, or null if successful</param>
		public void Login(string username, string password, Action<LoginResult, string> callback) {
			Dictionary<string, string> data = new Dictionary<string, string>(2) {
			{ "username", username },
			{ "password", password }
		};

			StartCoroutine(PostData("accounts/login.php", data, false, (www) => {
				try {
					JObject response = JObject.Parse(www.text);
					if (response["success"].Value<bool>()) {
						m_LoginToken = response["data"].Value<string>();

						APISettings settings = new APISettings() {
							authToken = m_LoginToken
						};

						string configFile = Path.Combine(Application.persistentDataPath, "api.json");
						File.WriteAllText(configFile, JsonConvert.SerializeObject(settings));

						callback(LoginResult.Success, null);
					} else {
						LoginResult result;
						switch (response["reason"].Value<string>()) {
						case "session_active":
							result = LoginResult.SessionActive;
							break;
						case "credentials":
							result = LoginResult.InvalidCredentials;
							break;
						case "client_error":
						case "server_error":
						default:
							result = LoginResult.Error;
							break;
						}
						callback(result, response["message"].ToObject<string>());
					}
				} catch (JsonException ex) {
					callback(LoginResult.Error, "Error communicating with server. (C1-2)");
					Debug.LogException(ex, this);
				}
			}));
		}

		/// <param name="callback">string is the human-readable error message</param>
		public void Logout(Action<LogoutResult, string> callback) {
			if (m_LoginToken == null) {
				throw new InvalidOperationException("Not logged in.");
			}

			StartCoroutine(PostData("accounts/logout.php", null, true, (www) => {
				try {
					JObject response = JObject.Parse(www.text);

					if (response["success"].Value<bool>()) {
						callback(LogoutResult.Success, null);
					} else {
						LogoutResult result;
						switch (response["reason"].Value<string>()) {
						case "token_invalid":
							result = LogoutResult.TokenInvalid;
							break;
						case "client_error":
						case "server_error":
						default:
							result = LogoutResult.Error;
							break;
						}
						callback(result, response["message"].ToObject<string>());
					}
				} catch (JsonException ex) {
					callback(LogoutResult.Error, "Error communicating with server. (C1-3)");
					Debug.LogException(ex, this);
				} finally {
					m_LoginToken = null; // Regardless of any errors, we should not stay logged in after this request.
				}
			}));
		}

		/// <param name="callback">string is the human-readable error message, or null if successful</param>
		public void IsLoggedIn(Action<LoginTestResult, string> callback) {
			if (m_LoginToken == null) {
				callback(LoginTestResult.NotLoggedIn, null);
			} else {
				StartCoroutine(PostData("accounts/testlogin.php", null, true, (www) => {
					try {
						JObject response = JObject.Parse(www.text);
						if (response["success"].Value<bool>()) {
							switch (response["data"].Value<string>()) {
							case "last_session_inactive":
								callback(LoginTestResult.LastSessionInactive, null);
								break;
							case "active":
								callback(LoginTestResult.Active, null);
								break;
							}
						} else {
							string errorMessage = response["message"].Value<string>();
							switch (response["reason"].Value<string>()) {
							case "session_active":
								callback(LoginTestResult.LastSessionActive, errorMessage);
								break;
							case "token_invalid":
								callback(LoginTestResult.Invalid, errorMessage);
								break;
							case "server_error":
							case "client_error":
							default:
								callback(LoginTestResult.Error, errorMessage);
								break;
							}
						}
					} catch (JsonException ex) {
						callback(LoginTestResult.Error, "Error communicating with server. (C1-3)");
						Debug.LogException(ex, this);
					}
				}));
			}
		}
		#endregion Account functions

		#region Ship functions
		/// <param name="callback">bool is success value, string is error message (null if successful)</param>
		public void GetShips(Action<bool, string, Ship[]> callback) {
			StartCoroutine(PostData("ship/getships.php", null, true, (www) => {
				try {
					JObject response = JObject.Parse(www.text);
					if (response["success"].Value<bool>()) {
						Ship[] ships = response["data"].ToObject<Ship[]>();
						callback(true, null, ships);
					} else {
						callback(false, response["message"].Value<string>(), null);
					}
				} catch (JsonException ex) {
					callback(false, "Error communicating with server. (C1-5)", null);
					Debug.LogException(ex, this);
				}
			}));
		}

		public void GetShips(int typeId, Action<bool, string, Ship[]> callback) {
			Dictionary<string, string> data = new Dictionary<string, string>(2) {
			{ "id", typeId.ToString() }
		};

			StartCoroutine(PostData("ship/buyship.php", data, true, (www) => {
				try {
					JObject response = JObject.Parse(www.text);
					if (response["success"].Value<bool>()) {
						Ship[] ships = response["data"].Value<Ship[]>();
						callback(true, null, ships);
					} else {
						callback(false, response["message"].Value<string>(), null);
					}
				} catch (JsonException ex) {
					callback(false, "Error communicating with server. (C1-6)", null);
					Debug.LogException(ex, this);
				}
			}));
		}

		public void GetShipTypes(Action<bool, string, ShipType[]> callback) {
			StartCoroutine(PostData("ship/listtypes.php", null, true, (www) => {
				try {
					JObject response = JObject.Parse(www.text);
					if (response["success"].Value<bool>()) {
						ShipType[] types = response["data"].Value<ShipType[]>();
						callback(true, null, types);
					} else {
						callback(false, response["message"].Value<string>(), null);
					}
				} catch (JsonException ex) {
					callback(false, "Error communicating with server. (C1-7)", null);
					Debug.LogException(ex, this);
				}
			}));
		}
		
		public void UpgradeShip(int shipId, Dictionary<string, int> upgradesToBuy, Action<bool, string, int> callback) {
			Dictionary<string, string> data = new Dictionary<string, string>(upgradesToBuy.Count + 1) {
				{ "id", shipId.ToString() }
			};

			foreach (KeyValuePair<string, int> item in upgradesToBuy) {
				if (item.Value != 0) {
					data.Add(item.Key, item.Value.ToString());
				}
			}

			StartCoroutine(PostData("ship/upgrade.php", data, true, (www) => {
				try {
					JObject response = JObject.Parse(www.text);
					if (response["success"].Value<bool>()) {
						int newMoney = response["data"].Value<int>();
						callback(true, null, newMoney);
					} else {
						callback(false, response["message"].Value<string>(), -1);
					}
				} catch (JsonException ex) {
					callback(false, "Error communicating with server. (C1-8)", -1);
					Debug.LogException(ex, this);
				}
			}));
		}
		#endregion Ship functions

		public void EndGame(int score, Action<bool, string, int> callback) {
			Dictionary<string, string> data = new Dictionary<string, string>() {
				{ "score", score.ToString() }
			};

			StartCoroutine(PostData("game/endgame.php", data, true, (www) => {
				try {
					JObject response = JObject.Parse(www.text);
					if (response["success"].Value<bool>()) {
						int newMoney = response["data"].Value<int>();
						callback(true, null, newMoney);
					} else {
						callback(false, response["message"].Value<string>(), -1);
					}
				} catch (JsonException ex) {
					callback(false, "Error communicating with server. (C1-9)", -1);
					Debug.LogException(ex, this);
				}
			}));
		}

		#region Enums
		public enum RegisterResult {
			Success, UsernameTaken, EmailTaken, Error
		}

		public enum LoginResult {
			Success, SessionActive, InvalidCredentials, Error
		}

		public enum LogoutResult {
			Success, TokenInvalid, Error
		}

		public enum LoginTestResult {
			/// <summary>
			/// Can log in: the last session is inactive.
			/// </summary>
			LastSessionInactive,
			/// <summary>
			/// Can't log in: the last session is active.
			/// </summary>
			LastSessionActive,
			/// <summary>
			/// This token is active
			/// </summary>
			Active,
			/// <summary>
			/// This token is invalid
			/// </summary>
			Invalid,
			NotLoggedIn,
			Error
		}

		#endregion Enums

		[Serializable]
		private class APISettings {
			public string authToken;
		}
	}
}
