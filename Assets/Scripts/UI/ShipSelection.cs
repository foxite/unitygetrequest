﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SpaceGrinder.UI {
	public class ShipSelection : MonoBehaviour {
		[SerializeField] private Image m_Icon;
		[SerializeField] private TMP_Text m_Title, m_Class;
		[SerializeField] private Button m_PlayButton;

		public void SetSelection(Ship ship) {
			if (ship != null) {
				m_PlayButton.interactable = true;
				m_Icon.color = Color.white;
				m_Title.text = ship.typeName + " " + ship.id;
				m_Class.text = ship.shipClass.ToString();
			} else {
				m_PlayButton.interactable = false;
				m_Icon.color = new Color(0, 0, 0, 0);
				m_Title.text = "";
				m_Class.text = "";
			}
		}
	}
}
