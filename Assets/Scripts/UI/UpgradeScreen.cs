﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SpaceGrinder.UI {
	public class UpgradeScreen : MonoBehaviour {
		[SerializeField] private TMP_Text m_ShipTitle;
		[SerializeField] private TMP_Text m_Hull, m_Armor, m_Speed, m_Kinetic, m_Laser, m_Missile, m_MissileCount;
		[SerializeField] private Button m_HullButton, m_ArmorButton, m_SpeedButton, m_KineticButton, m_LaserButton, m_MissileButton, m_MissileCountButton;
		[SerializeField] private Button m_ConfirmButton;
		[SerializeField] private GameObject m_UpgradeLoading;
		[SerializeField] private GameObject m_MainScreen;

		private Ship m_Ship;
		private Dictionary<string, int> m_UpgradesToBuy;

		private void Start() {
			m_HullButton.onClick.AddListener(() => {
				if (m_Ship != null) {
					int cost = m_Ship.hullUpgrades * 10;
					APIManager.Instance.Money -= cost;
					m_Ship.hullUpgrades++;
					m_UpgradesToBuy["hull"]++;
					UpdateValues();
				}
			});
			m_ArmorButton.onClick.AddListener(() => {
				if (m_Ship != null) {
					int cost = m_Ship.armorUpgrades * 10;
					APIManager.Instance.Money -= cost;
					m_Ship.armorUpgrades++;
					m_UpgradesToBuy["armor"]++;
					UpdateValues();
				}
			});
			m_SpeedButton.onClick.AddListener(() => {
				if (m_Ship != null) {
					int cost = m_Ship.speedUpgrades * 10;
					APIManager.Instance.Money -= cost;
					m_Ship.speedUpgrades++;
					m_UpgradesToBuy["speed"]++;
					UpdateValues();
				}
			});
			m_KineticButton.onClick.AddListener(() => {
				if (m_Ship != null) {
					int cost = m_Ship.kineticUpgrades * 10;
					APIManager.Instance.Money -= cost;
					m_Ship.kineticUpgrades++;
					m_UpgradesToBuy["kinetic"]++;
					UpdateValues();
				}
			});
			m_LaserButton.onClick.AddListener(() => {
				if (m_Ship != null) {
					int cost = m_Ship.laserUpgrades * 10;
					APIManager.Instance.Money -= cost;
					m_Ship.laserUpgrades++;
					m_UpgradesToBuy["laser"]++;
					UpdateValues();
				}
			});
			m_MissileButton.onClick.AddListener(() => {
				if (m_Ship != null) {
					int cost = m_Ship.missileUpgrades * 10;
					APIManager.Instance.Money -= cost;
					m_Ship.missileUpgrades++;
					m_UpgradesToBuy["missile"]++;
					UpdateValues();
				}
			});
			m_MissileCountButton.onClick.AddListener(() => {
				if (m_Ship != null) {
					int cost = m_Ship.missileCountUpgrades * 10;
					APIManager.Instance.Money -= cost;
					m_Ship.missileCountUpgrades++;
					m_UpgradesToBuy["missile_count"]++;
					UpdateValues();
				}
			});
		}

		public void SetShip(Ship ship) {
			m_Ship = ship;
			UpdateValues();
			m_UpgradesToBuy = new Dictionary<string, int>() {
				{ "hull", 0 },
				{ "armor", 0 },
				{ "speed", 0 },
				{ "kinetic", 0 },
				{ "laser", 0 },
				{ "missile", 0 },
				{ "missile_count", 0 }
			};
		}

		private void UpdateValues() {
			m_Hull.text = "Hull: " + m_Ship.hullUpgrades;
			m_Armor.text = "Armor: " + m_Ship.armorUpgrades;
			m_Speed.text = "Speed: " + m_Ship.speedUpgrades;
			m_Kinetic.text = "Kinetic: " + m_Ship.kineticUpgrades;
			m_Laser.text = "Laser: " + m_Ship.laserUpgrades;
			m_Missile.text = "Missile: " + m_Ship.missileUpgrades;
			m_MissileCount.text = "Missile Count: " + m_Ship.missileCountUpgrades;

			CalculateCost(m_HullButton, m_Ship.hullUpgrades);
			CalculateCost(m_ArmorButton, m_Ship.armorUpgrades);
			CalculateCost(m_SpeedButton, m_Ship.speedUpgrades);
			CalculateCost(m_KineticButton, m_Ship.kineticUpgrades);
			CalculateCost(m_LaserButton, m_Ship.laserUpgrades);
			CalculateCost(m_MissileButton, m_Ship.missileUpgrades);
			CalculateCost(m_MissileCountButton, m_Ship.missileCountUpgrades);
		}

		private void CalculateCost(Button btn, int upgrades) {
			int cost = upgrades * 10;
			btn.interactable = APIManager.Instance.Money >= cost;
		}

		public void ConfirmUpgrade() {
			m_UpgradeLoading.SetActive(true);
			m_ConfirmButton.interactable = false;

			APIManager.Instance.UpgradeShip(m_Ship.id, m_UpgradesToBuy, (success, message, newMoney) => {
				APIManager.Instance.Money = newMoney;
				if (success) {
					gameObject.SetActive(false);
					m_MainScreen.gameObject.SetActive(true);
					m_UpgradeLoading.SetActive(false);
					m_ConfirmButton.interactable = true;
				}
			});
		}
	}
}
