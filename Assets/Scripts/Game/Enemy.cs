﻿using UnityEngine;

namespace SpaceGrinder.Game {
	public class Enemy : PooledObject {
		private const float LookSpeed = 5.0f;

		private ObjectPool m_BulletPool;
		private EnemyPool m_SourcePool;
		private Player m_Player;

		private float m_Health;
		private float m_ShootPower;
		private float m_ShootSpeed;
		private float m_ShootDelay;
		private float m_TimeToNextShot;

		public Sprite m_BulletSprite;

		private void Start() {
			m_Player = Player.Instance;
		}
		
		private void FixedUpdate() {
			// http://answers.unity.com/answers/862619/view.html
			Vector3 direction = m_Player.transform.position - transform.position;
			Quaternion toRotation = Quaternion.FromToRotation(transform.forward, direction);
			transform.rotation = Quaternion.Lerp(transform.rotation, toRotation, LookSpeed * Time.time);

			m_TimeToNextShot -= Time.fixedDeltaTime;
			if (m_TimeToNextShot <= 0) {
				m_TimeToNextShot = m_ShootDelay;
				// Shoot
				Debug.Log("shoot");
				PooledObject bullet = m_BulletPool.TakeFromPool(transform.position, transform.forward.x, transform.forward.y, m_ShootSpeed, m_ShootPower);
				bullet.GetComponent<SpriteRenderer>().sprite = m_BulletSprite;
			}
		}

		public override void OnReturnToPool(ObjectPool pool) { }

		public override void OnTakeFromPool(ObjectPool pool, float xPos, float yPos, float zPos, params object[] params_) {
			transform.position = new Vector3(xPos, yPos, zPos);

			m_ShootDelay = Random.Range(0.1f, 0.5f);
			m_ShootPower = Random.Range(0.5f, 2f);
			m_ShootSpeed = Random.Range(5f, 10f);
			m_TimeToNextShot = m_ShootDelay;
		}

	}
}
