﻿using UnityEngine;

namespace SpaceGrinder.Game {
	public class Bullet : PooledObject {
		private float m_Speed;
		public float m_Power;

		private void Update() {
			transform.Translate(transform.forward * m_Speed * Time.deltaTime);
		}

		public override void OnReturnToPool(ObjectPool pool) { }

		/// <summary>
		/// Expected parameters: float xForward, float yForward, float speed, float power
		/// </summary>
		public override void OnTakeFromPool(ObjectPool pool, float xPos, float yPos, float zPos, params object[] params_) {
			transform.position = new Vector3(xPos, yPos, 0);
			transform.forward = new Vector3((float) params_[0], (float) params_[1], 0);
			m_Speed = (float) params_[2];
			m_Power = (float) params_[3];
		}
	}
}
