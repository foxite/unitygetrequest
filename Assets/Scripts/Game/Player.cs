﻿using UnityEngine;

namespace SpaceGrinder.Game {
	public class Player : Singleton<Player> {
		[SerializeField] private float m_Health;
		[SerializeField] private float m_TurnSpeed;
		[SerializeField] private float m_Acceleration;

		private Rigidbody2D m_RB;

		private void Start() {
			m_RB = GetComponent<Rigidbody2D>();
		}

		private void Update() {
			if (m_Health <= 0) {
				Debug.Log("DEAD!");
				// End game, submit score
			}

			m_RB.AddForce(transform.up * (Input.GetKey(KeyCode.W) ?  m_Acceleration : 0));
			m_RB.AddForce(transform.up * (Input.GetKey(KeyCode.S) ? -m_Acceleration : 0));
			
			if (Mathf.Abs(m_RB.velocity.magnitude) < 0.1f && !Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.S)) {
				m_RB.velocity = Vector2.zero;
			}
			
			m_RB.angularVelocity += Input.GetKey(KeyCode.A) ? m_TurnSpeed : 0;
			m_RB.angularVelocity -= Input.GetKey(KeyCode.D) ? m_TurnSpeed : 0;

			if (!Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.D)) {
				m_RB.angularVelocity = 0;
			}
		}

		private void OnCollisionEnter2D(Collision2D collision) {
			Bullet bullet;
			if (collision.gameObject.HasComponent(out bullet)) {
				m_Health -= bullet.m_Power;
			}
		}
	}
}
