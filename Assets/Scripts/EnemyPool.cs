﻿using System.Collections.Generic;
using SpaceGrinder.Game;
using UnityEngine;

namespace SpaceGrinder {
	public class EnemyPool : ObjectPool {
		[SerializeField] private List<Sprite> m_PossibleSprites;
		[SerializeField] private List<Sprite> m_BulletSprites;

		public override PooledObject TakeFromPool(float xPos, float yPos, float zPos, params object[] params_) {
			var ret = base.TakeFromPool(xPos, yPos, zPos, params_);
			ret.GetComponent<SpriteRenderer>().sprite = m_PossibleSprites[Random.Range(0, m_PossibleSprites.Count)];
			ret.GetComponent<Enemy>().m_BulletSprite = m_BulletSprites[Random.Range(0, m_BulletSprites.Count)];
			return ret;
		}
	}
}